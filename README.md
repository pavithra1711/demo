HTML E-commerce Online Shop

This is an online shop website template it is a multipurpose non-responsive website template that can cover the basic online shop requirement.
The website template is built on HTML5 CSS3 and Javascript / JQuery.
It has used external plugins library like jquery UI, fancybox for viewing product images on a detail page, and bxslider to slide products on the home page.
Also, it has used free stock images, icons, and google Roboto font.
Beautiful flat style HTML5 website template with clean code to edit easily well-commented sections.